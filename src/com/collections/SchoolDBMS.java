package com.collections;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SchoolDBMS {
    private String name;
    private Integer age;
    private Integer phoneNumber;
    static Scanner scanner = new Scanner(System.in);

    SchoolDBMS(String name, Integer age, Integer phoneNumber) {
        this.name = name;
        this.age = age;
        this.phoneNumber = phoneNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    private static void CurrentStudents(HashMap<Integer, SchoolDBMS> studentsInformation) {
        studentsInformation.put(1, new SchoolDBMS("Siva", 10, 6379912));
        studentsInformation.put(2, new SchoolDBMS("Soviya", 12, 7676899));
    }

    private static void AddStudentDetailsInDBMS(Integer rollNumber, HashMap<Integer, SchoolDBMS> studentsInformation) {
        ++rollNumber;
        System.out.println("Enter name, age and phone number ");
        String studentName = scanner.nextLine();
        scanner.nextLine();
        Integer studentAge = scanner.nextInt();
        Integer studentPhoneNumber = scanner.nextInt();
        studentsInformation.put(rollNumber, new SchoolDBMS(studentName, studentAge, studentPhoneNumber));
    }
    private static void UpdateStudentDetailsInDBMS(HashMap<Integer, SchoolDBMS> studentsInformation) {
        System.out.println("Enter roll number, name, age and phone number to update");
        Integer rollNumberToUpdate = scanner.nextInt();
        String nameToUpdate = scanner.nextLine();
        scanner.nextLine();
        Integer ageToUpdate = scanner.nextInt();
        Integer phoneNumberToUpdate = scanner.nextInt();
        studentsInformation.replace(rollNumberToUpdate, new SchoolDBMS(nameToUpdate, ageToUpdate, phoneNumberToUpdate));
        // studentsInformation.replace(rollNumberToUpdate,new SchoolManagementDBMS(this.getName(), this.getAge())));
    }
    private static void ShowDetails(HashMap<Integer, SchoolDBMS> studentsInformation) {
        for (Map.Entry<Integer, SchoolDBMS> pair : studentsInformation.entrySet()) {
            System.out.println("Roll number " + pair.getKey() + " Students Information( name, age, phone number) " + pair.getValue());
        }
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", age=" + age +
                ", phoneNumber=" + phoneNumber;
    }

    private static void RemoveStudentDetailsInDBMS(HashMap<Integer, SchoolDBMS> studentsInformation) {
        System.out.println("Enter roll number to delete");
        Integer rollNumber = scanner.nextInt();
        if (studentsInformation.containsKey(rollNumber)) {
            studentsInformation.remove(rollNumber);
        } else {
            System.out.println("Student with roll number " + rollNumber + " doesn't exist");
        }
    }

    public static void main(String[] args) {
        HashMap<Integer, SchoolDBMS> studentsInformation = new HashMap<>();
        Integer rollNumber = 2;
        CurrentStudents(studentsInformation);
        System.out.println("Enter your role to perform");
        String role = scanner.nextLine();
        if (role.equalsIgnoreCase("Admin")) {
            while (true) {
                System.out.println("What would you like to perform ? 1.Add 2.update 3.Remove 4.Show details");
                Integer process = scanner.nextInt();
                switch (process) {
                    case 1:
                        AddStudentDetailsInDBMS( rollNumber, studentsInformation);
                        break;
                    case 2:
                        UpdateStudentDetailsInDBMS(studentsInformation);
                        break;
                    case 3:
                        RemoveStudentDetailsInDBMS(studentsInformation);
                        break;
                    case 4:
                        ShowDetails(studentsInformation);
                        break;
                }
                if (process == 0) break;
            }
        } else {
            ShowDetails(studentsInformation);
        }
    }
}
